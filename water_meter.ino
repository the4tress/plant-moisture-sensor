/**
 * 
 * This script will monitor soil moisture and post the data to MQTT.
 * I used this sensor: HUABAN Soil Hygrometer Humidity Detection Module Moisture Water Sensor
 * ordered from here: https://www.amazon.com/gp/product/B077PW1VW5.
 * 
 * The script is modified from Adafruit's example on Github.
 * See license below.
 * 
 */


/***************************************************
  Adafruit MQTT Library Arbitrary Data Example

  Must use ESP8266 Arduino from:
    https://github.com/esp8266/Arduino

  Works great with Adafruit's Huzzah ESP board & Feather
  ----> https://www.adafruit.com/product/2471
  ----> https://www.adafruit.com/products/2821

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Stuart Feichtinger
  Modifed from the mqtt_esp8266 example written by Tony DiCola for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
****************************************************/


#include <ESP8266WiFi.h>                // Include the Wi-Fi library
#include "Adafruit_MQTT.h"              // Include MQTT library
#include "Adafruit_MQTT_Client.h"

/************************ WIFI CONFIG ******************************/

#define WIFI_SSID ""                    // The SSID (name) of the Wi-Fi network you want to connect to
#define WIFI_PASS ""                    // The password of the Wi-Fi network


/************************ MQTT CONFIG ******************************/

#define MQTT_ADDR ""                    // mqtt server address
#define MQTT_PORT 1883                  // port; use 8883 for SSL
#define MQTT_USER ""                    // username
#define MQTT_PASS ""                    // password
#define MQTT_FEED "/sensor/1"           // name of topic
#define MQTT_FREQ 1000                  // post frequency in milliseconds

/************************ DO NOT EDIT BELOW HERE ******************************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, MQTT_ADDR, MQTT_PORT, MQTT_USER, MQTT_PASS);
Adafruit_MQTT_Publish ap = Adafruit_MQTT_Publish(&mqtt, MQTT_FEED);

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void setup() {
  Serial.begin(9600);         // Start the Serial communication to send messages to the computer
  delay(1000);
  
  // Connect to wifi 
  WiFi.begin(WIFI_SSID, WIFI_PASS);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID); Serial.print(" .");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {       // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print('.');
  }

  Serial.println("\nConnection established!\n");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
  Serial.println("\n");
  
  delay(2000);

  // Connect to MQTT
  MQTT_connect();

  // Monitor data
  Serial.print("Monitoring pin A0 every ");
  Serial.print(MQTT_FREQ);
  Serial.println(" ms ...");
}

// the loop routine runs over and over again forever:
void loop() {
  // Make sure MQTT is still connected
  MQTT_connect();
  
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  
  // print out the value you read:
  Serial.println(sensorValue);
  
  // publish the value to MQTT
  ap.publish(sensorValue);

  delay(MQTT_FREQ);        // delay in between reads for stability
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print(F("Connecting to MQTT server at "));
  Serial.print(MQTT_ADDR); Serial.print(":");
  Serial.print(MQTT_PORT); Serial.println(" ...");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println(F("Retrying MQTT connection in 5 seconds..."));
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println(F("MQTT Connected!\n"));
}
